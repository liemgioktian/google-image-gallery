<?php

add_action( 'add_meta_boxes', function () {
  add_meta_box( 'google-image-gallery', __( 'Google Image Gallery', 'google-image-galery' ), function ( $post ) {
    include GOOGLE_IMAGE_GALLERY_PATH . 'meta-box.php';
  }, 'post' );
});

add_filter( 'content_save_pre' , function ($content) {
  if (isset ($_POST['google-image-gallery-images'])) {
    $images = array();

    if (strlen ($content) > 0) {
      if (!class_exists( 'simple_html_dom_node'))
        include GOOGLE_IMAGE_GALLERY_PATH . 'simplehtmldom/simple_html_dom.php';
      $existing = str_get_html($content);
      foreach($existing->find('ul li img') as $img) 
        $images[] = str_replace('\"', '', $img->src);
    }

    $images   = array_merge($images, $_POST['google-image-gallery-images']);
    $items    = '';
    foreach ($images as $url) $items .= '<li class="google-image-gallery-li"><img src="'.$url.'" /></li>';
    $content  = '<ul id="google-image-gallery-ul">'.$items.'</ul>';
  }
  return $content;
}, 10, 1);

add_action( 'admin_footer-post-new.php', 'google_image_gallery_js' );

add_action( 'admin_footer-post.php', 'google_image_gallery_js' );

add_action( 'wp_ajax_google_image_gallery', function () {
  if (!class_exists( 'WP_Http' )) include_once( ABSPATH . WPINC . '/class-http.php' );
  require_once (GOOGLE_IMAGE_GALLERY_PATH . 'simplehtmldom/simple_html_dom.php');

  $http     = new WP_Http();
  $parser   = new simple_html_dom();
  $userAgent= 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36';

  $param    = $_GET;
  $query    = str_replace(' ', '+', $param['q']);
  $num      = $param['num'];
  $size     = $param['size'];
  $size     = '0' == $size ? 'tbas=0' : "tbs=isz:$size";
  $start    = 0;
  $result   = array();

  while ($start < $num) {
    $response   = $http->request("https://www.google.com/search?tbm=isch&q=$query&num=$num&start=$start&$size", array('user-agent' => $userAgent));
    $parser->load($response['body'], true, false);
    foreach ($parser->find('div[class^="rg_meta"]') as $meta) {
      $obj      = json_decode($meta->innertext);
      $result[] = $obj->ou;
    }
    $start += 20;
  }

  $result = array_slice($result, 0, $num);
  die(json_encode($result));
});

add_action( 'the_post', function () {
  $config= (int) get_option('google-image-gallery-item-per-row');
  $config= 0 === $config ? 4 : $config;
  $width = 100 / $config . '%';
  $css   = '<style type="text/css">{{css}}</style>';
  $style = "li.google-image-gallery-li {
    width: $width;
    float: left;
    list-style: none;
  }";
  $css = str_replace('{{css}}', $style, $css);
  echo $css;
});

add_action('admin_menu', function () {
  add_menu_page('Google Image Gallery', 'Google Image Gallery', 'manage_options', 'google-image-gallery', function () {
    include GOOGLE_IMAGE_GALLERY_PATH . 'config.php';
  });
});

function google_image_gallery_js () {
  echo '<script type="text/javascript" src="'.GOOGLE_IMAGE_GALLERY_URL.'google-image-gallery.js"></script>';
}