<style type="text/css">
  .google-image-gallery-metabox {
    width: 100%;
  }
  .google-image-gallery-metabox td {
    text-align: center;
  }
  .google-image-gallery-images {
    margin: 0 auto;
  }
  .google-image-gallery-images img {
    max-width: 175px;
    max-height: 175px;
  }
</style>
<div>
  <table class="google-image-gallery-metabox">
    <tr>
      <td>
        <input type="text" placeholder="Search Images.." class="google-image-gallery-input">
        <input type="text" class="google-image-gallery-count" value="20" />
        <select class="google-image-gallery-size" >
          <option value="0">Any Size</option>
          <option value="l">Large</option>
          <option value="m">Medium</option>
          <option value="i">Icon</option>
        </select>
        <a class="button button-primary google-image-gallery-button" data-action="<?= GOOGLE_IMAGE_GALLERY_LOCAL_API ?>">search</a>
        <input type="checkbox" class="google-image-gallery-select-all" /> select all
      </td>
    </tr>
    <tr>
      <td class="google-image-gallery-container"></td>
    </tr>
  </table>
</div>