jQuery(function () {
  var imgPerRow   = 4
  jQuery('.google-image-gallery-button').click(function () {
    var $this     = jQuery(this)
    var $btn_txt  = $this.html()
    var $input    = jQuery('.google-image-gallery-input')
    var $num      = jQuery('.google-image-gallery-count')
    var $size     = jQuery('.google-image-gallery-size')
    var $container= jQuery('.google-image-gallery-container')
    var google_image_gallery_local_api = $this.attr('data-action')
    $this.html('Please Wait..')
    jQuery.get(
      ajaxurl, 
      {action:'google_image_gallery', q: $input.val(), num: $num.val(), size: $size.val()},
      function (img) {
        $this.html($btn_txt)
        var images = JSON.parse(img)
        var $html = '<table class="google-image-gallery-images">'
        for (var i in images) {
          var begin = parseInt(i) % imgPerRow === 0
          var end = parseInt(i) % imgPerRow === imgPerRow -  1
          if (begin) $html += '<tr>'
          $html += '<td>'
          $html += '<img src="'+images[i]+'">'
          $html += '<br/>'
          $html += '<input type="checkbox" name="google-image-gallery-images[]" value="'+images[i]+'" />'
          $html +='<td>'
          if (end) $html += '</tr>'
        }
        $html += '</table>'
        $container.html($html)
      }
    )
  })
  jQuery('.google-image-gallery-select-all').click(function () {
    var $this  = jQuery(this)
    var $state = '.google-image-gallery-container input:checkbox'
    if ($this.is(':checked')) jQuery($state).not(':checked').click()
    else jQuery($state + ':checked').click()
  })
})