<?php

  if (isset ($_POST['google-image-gallery-item-per-row'])) {
    update_option ('google-image-gallery-item-per-row', $_POST['google-image-gallery-item-per-row']);
  }

?>
<div class="wrap">
  <H1>Google Image Gallery Settings</H1>
  <form method="POST">
    <table class="form-table">

      <?php if ($_POST): ?>
      <caption>
        <div class="notice notice-success is-dismissible">
          <p>Changes Saved</p>
        </div>
      </caption>
      <?php endif ?>

      <tbody>

        <tr>
          <th scope="row">
            <label for="google-image-gallery-item-per-row">Image Item Per Row</label>
          </th>
          <td>
            <input name="google-image-gallery-item-per-row" type="text" class="regular-text" value="<?= get_option('google-image-gallery-item-per-row') ?>">
          </td>
        </tr>

        <tr>
          <th scope="row"></th>
          <td>
            <input type="submit" class="button button-primary" value="Save Changes">
          </td>
        </tr>

      </tbody>

    </table>
  </form>
</div>