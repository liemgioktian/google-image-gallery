<?php 
/*
Plugin Name: Google image gallery
URI: https://bitbucket.org/liemgioktian/google-image-gallery
Description: Build image gallery directly from google image
Author: henrisusanto 081901088918
Version: 1.0
Author URI: https://bitbucket.org/liemgioktian/
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.txt
*/

define('GOOGLE_IMAGE_GALLERY_PATH',  plugin_dir_path( __FILE__ ));
define('GOOGLE_IMAGE_GALLERY_URL', plugin_dir_url(__FILE__));
define('GOOGLE_IMAGE_GALLERY_LOCAL_API', site_url('wp-json/google-image-gallery/search'));

include GOOGLE_IMAGE_GALLERY_PATH . 'core.php';